""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins			
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('$HOME/.config/nvim/plugged')

	" gruvbox colorscheme. Seems to work the best for me.
	Plug 'morhetz/gruvbox'

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => User Interface			
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" set default color scheme
colorscheme gruvbox
autocmd vimenter * ++nested colorscheme gruvbox

" enable numpers
set number
